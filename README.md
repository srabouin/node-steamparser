# steam parser #

### What is this for? What does it do? ###

* Converts your iOS Steam Mobile configuration file (Steamguard-XXXXXXXXXXXXXXXX) to a readable format.

### How do I get set up? ###

* Assuming you have nodejs, npm and git installed and at least basic knowledge of command line.
* Run the following commands, assuming the Steamguard file is in your home folder:
```sh
git clone https://bitbucket.org/srabouin/node-steamparser.git
cd node-steamparser
npm install
node ios ~/Steamguard-XXXXXXXXXXXXXXXX
```

### Who do I talk to if I run into problems, want to report a bug, or want to suggest features? ###

* Please use the [issues](https://bitbucket.org/srabouin/node-steamparser/issues?status=new&status=open) section of this repo for bug reports and feature suggestions.
