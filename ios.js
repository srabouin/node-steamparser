var bplist = require('bplist-parser');
var fs = require('fs');
var CookieParser = require('binary-cookies');
var p = CookieParser();
var steamkeys = ['shared_secret', 'secret_1', 'identity_secret', 'revocation_code', 'serial_number', 'steamguard_scheme', 'steamid', 'account_name', 'token_gid'];

console.log("iOS Steam plist converter v%s\n", require('./package.json').version);

if(process.argv[2]) {
	fs.stat(process.argv[2], function(err, stat) {
	    if(err == null) {
			bplist.parseFile(process.argv[2], function(err, plist) {
				if (err) {
		  			throw err;
				}

				for (var key in plist[0]) {
					if (steamkeys.indexOf(plist[0][key]) !== -1) {
						console.log(plist[0][key], ':', plist[0][parseInt(key) + 12]);
					} 
				}

			    if(process.argv[3]) {
			    	console.log('Parsing cookies...');
			    	p.parse(process.argv[3], function(err, cookies) {
						console.log(cookies);
					});
			    }
			});
	    } else {
	        console.log('Error opening "%s": %s', process.argv[2], err.code);
	    }
	});
} else {
	console.log("\nError, no filename specified.\nUsage: \n        node ios [filename]\n");
}
